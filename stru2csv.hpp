#ifndef STRU2CSV_HPP
#define STRU2CSV_HPP
#include <stdio.h>
#include <vector>

#define TYPE(T, F) {&F, #F, print_ ## T}
struct desc {
    void* addr;
    const char* name;
    void (*foo)(FILE*, void*, int);
};
void print_header(FILE* fp, std::vector<desc> d);
int stru2csv(FILE* fp, int idx0, int idx1, std::vector<desc> dList);
void print_int32_t(FILE* fp, void* addr, int offset);
void print_int16_t(FILE* fp, void* addr, int offset);

#include "stru2csv.cpp"

#endif
