#include "stru2csv.hpp"
#include <stdint.h>
#include <vector>

int stru2csv(FILE* fp, int idx0, int idx1, std::vector<desc> dList)
{
    print_header(fp, dList);

    for (int i = idx0; i < idx1; i++)
    {
        for (int j = 0; j < dList.size(); j++)
        {
            dList[j].foo(fp, dList[j].addr, i);
        }
        fprintf(fp, "\n");
    }
    return 0;
}

void print_header(FILE* fp, std::vector<desc> d)
{
    for (int j = 0; j < d.size(); j++)
    {
        fprintf(fp, "%s;", d[j].name);
    }
    fprintf(fp, "\n");

}
void print_int32_t(FILE* fp, void* addr, int offset)
{
    fprintf(fp, "%d;", *(((int32_t*)addr) + offset));
}

void print_int16_t(FILE* fp, void* addr, int offset)
{
    fprintf(fp, "%d;", *(((int16_t*)addr) + offset));
}
