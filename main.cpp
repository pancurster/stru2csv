#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "stru2csv.hpp"

const int32_t ue_max = 16;
const int32_t gather_mask_size_max = 256;

typedef struct {
    int32_t gather_size[ue_max];
    int32_t gather_mask_offset[ue_max];
    int32_t gather_mask[gather_mask_size_max];
    int16_t n_llr[ue_max];
    int16_t llr_offset[ue_max];
    int8_t  n_ue;
} bit_t;

int main(int argc, char *argv[])
{
    FILE * fp = fopen("out.csv", "w");
    bit_t bit;
    memset(&bit, 0, sizeof(bit));
    bit.gather_size[0] = 13;
    bit.gather_size[1] = 45;
    bit.llr_offset[0] = 10;
    bit.llr_offset[1] = 123;

    stru2csv(fp, 0, ue_max,
       {
        TYPE(int32_t, bit.gather_size),
        TYPE(int32_t, bit.gather_size),
        TYPE(int32_t, bit.gather_mask_offset),
        TYPE(int16_t, bit.n_llr),
        TYPE(int16_t, bit.llr_offset)
       }
    );
    stru2csv(fp, 0, gather_mask_size_max,
        {
            TYPE(int32_t, bit.gather_mask)
        }
    );

    fclose(fp);
    return 0;
}
